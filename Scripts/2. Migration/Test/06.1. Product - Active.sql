--SET NOCOUNT ON;

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2' 
go
*/



-- Update staging table
-- 1319
UPDATE p
	SET p.IsActive = 
		CASE WHEN pp.[Status] = 'Active' THEN 'TRUE'
			ELSE 'FALSE'
		 END 
FROM [Migration_DataUpdate].dbo.Product2 p	
JOIN [Migration_DataUpdate].dbo.ProductPivot pp on pp.[Vision Product ID] = p.External_Id__c




-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Product2
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Product2'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Product2'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[Product2] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Product2 a
JOIN SALESFORCE_PROD.dbo.Product2 aa on aa.External_Id__c = a.External_Id__c

