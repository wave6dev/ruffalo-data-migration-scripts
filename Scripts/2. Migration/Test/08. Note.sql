--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Note;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
go
*/


-- Create staging table
-- 
-- 7
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,c.Id as ParentId
	,cr.ContactID
	,'Customer Reference: ' + ISNULL(cr.custReference, cr.ContactID) as Title
	,CAST([dbo].[udf_StripHTML](cr.custReferenceNotes) as nvarchar(MAX)) as Body
INTO [Migration_DataUpdate].dbo.Note 	
FROM [Migration_Source].dbo.Contacts_Reference cr
INNER JOIN SALESFORCE_PROD.dbo.Contact c ON c.ContactID__c = cr.ContactID
--INNER JOIN [Migration_Source].dbo.Contacts c ON c.ContactID = cr.ContactID
ORDER BY cr.ContactID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Note
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Note'



--------------------
-- CONVERT NOTES ---
--------------------

-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Note'


-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'ContentNote'