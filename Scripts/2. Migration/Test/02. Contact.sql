--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Contact;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
go
*/




-- Create staging table
-- 140175
-- 309
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,c.ContactID as ContactID__c
	,a.Id as AccountId
	,CASE WHEN cpc.CustPrimary IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary__c
	,c.LastName as LastName
	,c.FirstName as FirstName
	,c.MiddleName as MiddleName
	,c.Salutation as Salutation
	,c.Suffix as Suffix
	,c.Title as Title
	,c.[Type] as Type__c
	,ISNULL(c.Address1, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address2, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address3, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address4, '') as MailingStreet
	,c.City as MailingCity
	,CAST(c.[State] as nvarchar(100)) as MailingState
	,c.Zip as MailingPostalCode
	,CAST(c.Country as nvarchar(100)) as MailingCountry
	,c.Phone as Phone
	,c.Fax as Fax
	,c.CellPhone as MobilePhone
	,c.Email as Email
	,c.MailingAddress as Mailing_Address__c
	,c.Billing as Billing__c
	,c.ContactStatus as Status__c
	,c.PreferredName as Preferred_Name__c
	,CASE WHEN cctf.CustPrimaryEM IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary_EM__c
	,CASE WHEN cctf.CustPrimaryFM IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary_FM__c
	,cctf.custActiveReference as Active_Reference__c
	,cctf.custNewtoposition as New_To_Position__c
	,CASE WHEN cctf.custNoEmail IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as No_Email__c
	,CASE WHEN cctf.custNoMail IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as No_Mail__c
	,cctf.custNoEmailDate as No_Email_Date__c
	,cctf.custNoMailDate as No_Mail_Date__
	,c.CreateDate as CreatedDate
	,u.Id as CreatedByID
	,c.ModDate as LastModifiedDate
	,u2.Id as LastModifiedByID
INTO [Migration_DataUpdate].dbo.Contact 	
FROM [Migration_Source].dbo.Contacts c
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = c.ClientID
--INNER JOIN Migration_DataUpdate.dbo.Account a ON a.DeltekID__c = c.ClientID   -- change this
-- dedupe the emails
LEFT JOIN Migration_Source.dbo.Clients_PrimaryContacts cpc on cpc.CustContact = c.ContactID
LEFT JOIN Migration_Source.dbo.ContactCustomTabFields cctf on cctf.ContactID = c.ContactID
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.EmployeeID__c = c.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = c.ModUser
WHERE ContactStatus = 'A'
AND c.EMail IS NOT NULL
AND c.EMail NOT LIKE '%.rnl'
AND c.EMail NOT LIKE '%ruffalonl.com%'
AND c.[Type] != 'V'
ORDER BY c.ClientID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Contact
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix system users if null
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET CreatedByID = @@DataUser
WHERE CreatedByID IS NULL

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET LastModifiedByID = @@DataUser
WHERE LastModifiedByID IS NULL

-- Fix US Country
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET MailingCountry = 'United States'
WHERE MailingCountry = 'US'

-- Fix US States
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET MailingState = 
CASE WHEN MailingState = 'AL' THEN 'Alabama'
	WHEN MailingState = 'AK' THEN 'Alaska'
	WHEN MailingState = 'AZ' THEN 'Arizona'
	WHEN MailingState = 'AR' THEN 'Arkansas'
	WHEN MailingState = 'CA' THEN 'California'
	WHEN MailingState = 'CO' THEN 'Colorado'
	WHEN MailingState = 'CT' THEN 'Connecticut'
	WHEN MailingState = 'DE' THEN 'Delaware'
	WHEN MailingState = 'FL' THEN 'Florida'
	WHEN MailingState = 'GA' THEN 'Georgia'
	WHEN MailingState = 'HI' THEN 'Hawaii'
	WHEN MailingState = 'ID' THEN 'Idaho'
	WHEN MailingState = 'IL' THEN 'Illinois'
	WHEN MailingState = 'IN' THEN 'Indiana'
	WHEN MailingState = 'IA' THEN 'Iowa'
	WHEN MailingState = 'KS' THEN 'Kansas'
	WHEN MailingState = 'KY' THEN 'Kentucky'
	WHEN MailingState = 'LA' THEN 'Louisiana'
	WHEN MailingState = 'ME' THEN 'Maine'
	WHEN MailingState = 'MD' THEN 'Maryland'
	WHEN MailingState = 'MA' THEN 'Massachusetts'
	WHEN MailingState = 'MI' THEN 'Michigan'
	WHEN MailingState = 'MN' THEN 'Minnesota'
	WHEN MailingState = 'MS' THEN 'Mississippi'
	WHEN MailingState = 'MO' THEN 'Missouri'
	WHEN MailingState = 'MT' THEN 'Montana'
	WHEN MailingState = 'NE' THEN 'Nebraska'
	WHEN MailingState = 'NV' THEN 'Nevada'
	WHEN MailingState = 'NH' THEN 'New Hampshire'
	WHEN MailingState = 'NJ' THEN 'New Jersey'
	WHEN MailingState = 'NM' THEN 'New Mexico'
	WHEN MailingState = 'NY' THEN 'New York'
	WHEN MailingState = 'NC' THEN 'North Carolina'
	WHEN MailingState = 'ND' THEN 'North Dakota'
	WHEN MailingState = 'OH' THEN 'Ohio'
	WHEN MailingState = 'OK' THEN 'Oklahoma'
	WHEN MailingState = 'OR' THEN 'Oregon'
	WHEN MailingState = 'PA' THEN 'Pennsylvania'
	WHEN MailingState = 'RI' THEN 'Rhode Island'
	WHEN MailingState = 'SC' THEN 'South Carolina'
	WHEN MailingState = 'SD' THEN 'South Dakota'
	WHEN MailingState = 'TN' THEN 'Tennessee'
	WHEN MailingState = 'TX' THEN 'Texas'
	WHEN MailingState = 'UT' THEN 'Utah'
	WHEN MailingState = 'VT' THEN 'Vermont'
	WHEN MailingState = 'VA' THEN 'Virginia'
	WHEN MailingState = 'WA' THEN 'Washington'
	WHEN MailingState = 'WV' THEN 'West Virginia'
	WHEN MailingState = 'WI' THEN 'Wisconsin'
	WHEN MailingState = 'WY' THEN 'Wyoming'
END


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'Contact'
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact'
exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact', 'ContactID__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Contact'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [ContactID__c] ON [dbo].[Contact] ([ContactID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Contact a
JOIN SALESFORCE_PROD.dbo.Contact aa on aa.ContactID__c = a.ContactID__c