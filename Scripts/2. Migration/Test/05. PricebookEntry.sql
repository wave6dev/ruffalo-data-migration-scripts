--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.PricebookEntry;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'PricebookEntry' 
go
*/



DECLARE @@stdPB as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.Pricebook2 WHERE [Name] = 'Standard Price Book')

-- Create staging table
-- 1319
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,[Vision Product ID] as [Vision Product ID]
	,pp.Id as Product2ID
	,@@stdPB as Pricebook2ID
	,p.[List Price] as UnitPrice
	,CAST('TRUE' as nvarchar(20)) as IsActive		-- true to load in inactivate later
	/*,CASE WHEN p.[Status] = 'Active' THEN 'TRUE'
		ELSE 'FALSE'
	 END as IsActive*/
INTO [Migration_DataUpdate].dbo.PricebookEntry 	
FROM [Migration_DataUpdate].dbo.ProductPivot p
INNER JOIN SALESFORCE_PROD.dbo.Product2 pp on pp.External_Id__c = p.[Vision Product ID]
ORDER BY p.[Vision Product ID]



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].PricebookEntry
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'PricebookEntry'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'PricebookEntry'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'PricebookEntry'


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.PricebookEntry a
JOIN SALESFORCE_PROD.dbo.PricebookEntry aa on aa.Product2ID = a.Product2ID and aa.Pricebook2Id = a.Pricebook2ID