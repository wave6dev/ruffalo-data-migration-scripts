--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Opportunity;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c' 
go
*/


DECLARE @@stdPB as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.Pricebook2 WHERE [Name] = 'Standard Price Book')
DECLARE @@opRT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Opportunity')
DECLARE @@opCORT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Change Order')


-- Create staging table
-- 61754
-- 233
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,o.OpportunityID as OpportunityID__c
	,a.Id as AccountId
	,@@stdPB as Pricebook2Id
	,o.[Name] as [Name]
	,o.OpportunityType as Type
	,CASE WHEN o.Stage = 'LeadGen' THEN 'Lead Generating'
		WHEN o.Stage = 'Engaging' THEN 'Engaging'
		WHEN o.Stage = 'Value' THEN 'Planning for Value'
		WHEN o.Stage = 'DrvngClose' THEN 'Driving to Close'
		WHEN o.Stage = 'Committed' THEN 'Contract Negotiation'
		WHEN o.Stage = 'Won' THEN 'Won'
		WHEN o.Stage = 'Lost' Then 'Lost'
	 END as StageName
	,o.Revenue as Amount
	,CAST([dbo].[udf_StripHTML](o.[Description]) as nvarchar(MAX)) as [Description]
	,CAST(o.Probability as decimal(18,0)) as Probability
	,ISNULL(o.CloseDate, o.ModDate) as CloseDate
	,c.Id as Contact__c
	,CASE WHEN occf.CustChangeOrderIndicator = 'Y' THEN @@opCORT
		ELSE @@opRT
	 END as RecordTypeID
	,CASE WHEN occf.CustChangeOrderIndicator = 'Y' THEN 'TRUE'
		ELSE 'FALSE'
	 END as Change_Order__c
	,o.[Source] as LeadSource
	,CASE WHEN o.ClosedReason = 'Alternate' THEN 'Alternative RNL Product'
		WHEN o.ClosedReason = 'Budget' THEN 'Budget / Funding'
		WHEN o.ClosedReason = 'Cancelled' THEN 'Cancelled'
		WHEN o.ClosedReason = 'Leadership' THEN 'Change in Leadership'
		WHEN o.ClosedReason = 'Competitor' THEN 'Competitor better fit'
	    WHEN o.ClosedReason = 'Price' THEN 'Competitor better pricing'
		WHEN o.ClosedReason = 'Features' THEN 'Could not meet needs'
		WHEN o.ClosedReason = 'Prevend' THEN 'Preferred Vendor'
		WHEN o.ClosedReason = 'UnqualFSI' Then 'Disqualified FSI'
		WHEN o.ClosedReason = 'Eliminate' Then 'Eliminating Channel'
		WHEN o.ClosedReason = 'Existing' Then 'Existing Project'
		WHEN o.ClosedReason = 'Internal' Then 'Internal / In-house solution'
		WHEN o.ClosedReason = 'Merged' Then 'Merged Opportunity'
		WHEN o.ClosedReason = 'Unresponsi' Then 'No Response'
		WHEN o.ClosedReason = 'Noncomplia' Then 'Non-compliance'
		WHEN o.ClosedReason = 'NotNow' Then 'Not interested at this time'
		WHEN o.ClosedReason = 'Timing' Then 'Purchase delayed'
		WHEN o.ClosedReason = 'RNLNoGO' Then 'RNL Declined to Pursue'
		WHEN o.ClosedReason = 'Unsatisfy' Then 'Unhappy'
		ELSE o.ClosedReason
	 END Loss_Reason__c
	,CAST(o.ClosedNotes as nvarchar(MAX)) as Closed_Notes__c
	,occf.CustExpectedCloseDate as Exp_Closed_Date__c
	,CASE WHEN occf.CustRequestforContract = 'Y' THEN 'TRUE'
		ELSE 'FALSE'
	 END as Request_For_Contract__c
	,occf.CustTier as Tier__c
	,CAST(occf.CustNextSteps as nvarchar(255)) as NextStep
	,occf.CustAnticipatedDecisionDate as Anticipated_Decision_Date__c
	,occf.CustBAFONegotiations as BAFO_Negotiations__c
	,occf.CustBidQuestions as Bid_Questions__c
	,CASE WHEN ISNULL(occf.CustDeliveryTimelineDates, '') = 'Y' THEN 'Delivery Timeline Dates;'
         ELSE '' 
		END +
		CASE WHEN ISNULL(occf.CustDueDateTimeline, '') = 'Y' THEN 'Due Date Timeline;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustBudget, '') = 'Y' THEN 'Budget;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustNocampusrelationship, '') = 'Y' THEN 'No Campus Relationship;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustReasonforNotResponding, '') = 'Y' THEN 'Out of Scope;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustPoorCampusRelationship, '') = 'Y' THEN 'Poor Campus Relationship;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustUniformedScope, '') = 'Y' THEN 'Uninformed Scope;'
			ELSE ''
		END +
		CASE WHEN ISNULL(occf.CustWrittenforComp, '') = 'Y' THEN 'Written for Comp;'
			ELSE ''
	 END as Reason_For_Not_Responding__c
	,occf.CustDateDue as Date_Due__c
	,ucew.Id as Proposal_Manager__c
	,occf.CustIntenttoAward as Intent_to_Award__c
	,occf.CustMethodofReceipt as Method_of_Receipt__c
	,CAST(ISNULL(occf.CustNextStepsNegotiationNotes, '') 
		+ ' ' + ISNULL(occf.CustEMOriginalSoliticationAttach, '')
		+ ' ' + ISNULL(occf.CustNotes, '') as NVARCHAR(MAX))
		as Notes__c
	,occf.CustNoBidLetter as No_Bid_Letter__c
	,occf.CustPresentation as Presentation__c
	,occf.CustProposalSent as Original_Proposal_Sent__c
	,occf.CustProposalValidityPeriod as Proposal_Validity_Period__c
	,occf.CustQuestionsDue as Questions_Due__c
	,occf.CustReasonforRebid as Reason_for_Rebid_only_if_Rebid__c
	,occf.CustReceivedDate as Received_Date__c
	,occf.CustRespond as No_Response__c
	,occf.CustRevisedProposalSent as Proposal_Sent__c
	,occf.CustRFPShaping as RFP_Shaping__c
	,occf.CustShipDate as Ship_Date__c
	,occf.CustSoleSource as Sole_Source__c
	,occf.CustBidName as Solicitation_Name_Number__c
	,CASE WHEN occf.CustSource IN ('Blind','Historical','Meeting/Demo','OA','Renewal', 'Touch Point') THEN occf.CustSource
		ELSE NULL
	 END ProposalSource__c
	,occf.CustVersionType as Document_Type__c
	,o.CreateDate as CreatedDate
	,u.Id as CreatedByID
	,o.ModDate as LastModifiedDate
	,u2.Id as LastModifiedByID
INTO [Migration_DataUpdate].dbo.Opportunity 	
FROM [Migration_Source].dbo.Opportunity o
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = o.ClientID
LEFT JOIN SALESFORCE_PROD.dbo.Contact c on c.ContactID__c = o.ContactID
LEFT JOIN SALESFORCE_PROD.dbo.Project__c p on p.Project_Number__c = occf.CustProjectPlaceholder
LEFT JOIN Migration_Source.dbo.OpportunityCustomTabFields occf on occf.OpportunityID = o.OpportunityID
LEFT JOIN SALESFORCE_PROD.dbo.[User] ucew on ucew.EmployeeID__c = occf.CustEmailwriter
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.EmployeeID__c = o.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = o.ModUser
ORDER BY o.OpportunityID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Opportunity
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Opportunity'
exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Opportunity', 'OpportunityID__c'
--exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Opportunity'




-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [OpportunityID__c] ON [dbo].[Opportunity] ([OpportunityID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Opportunity a
JOIN SALESFORCE_PROD.dbo.Opportunity aa on aa.OpportunityID__c = a.OpportunityID__c