--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Task;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
go
*/



-- Create staging table
-- 689016, 1min
-- 276
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,a.ActivityID as External_Id__c
	,o.Id as WhatID
	,a.ContactID as LegacyContactID
	,a.[Type] as [Type]
	,a.[Subject] as [Subject]
	,u.Id as OwnerID
	,CASE WHEN a.TaskStatus = 'Not Started' THEN 'Open'
		ELSE 'Completed'
	END as [Status]
	,a.TaskCompletionDate as Completed_Date__c
	,a.[Priority] as [Priority]
	,CAST([dbo].[udf_StripHTML](a.Notes) as nvarchar(MAX)) as [Description]
	,a.CreateDate as CreatedDate
	,u2.Id as CreatedByID
	,a.ModDate as LastModifiedDate
	,u3.Id as LastModifiedByID
INTO [Migration_DataUpdate].dbo.Task 	
FROM [Migration_Source].dbo.Activity a
--LEFT JOIN SALESFORCE_PROD.dbo.Opportunity o on o.OpportunityID__c = a.OpportunityID
INNER JOIN SALESFORCE_PROD.dbo.Opportunity o on o.OpportunityID__c = a.OpportunityID
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.EmployeeID__c = a.Employee
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = a.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u3 on u3.EmployeeID__c = a.ModUser
--WHERE PAST YEAR
ORDER BY a.ActivityID DESC



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Task
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix system users if null
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Task 	
SET OwnerID = @@DataUser
WHERE OwnerID IS NULL

UPDATE [Migration_DataUpdate].dbo.Task 	
SET CreatedByID = @@DataUser
WHERE CreatedByID IS NULL

UPDATE [Migration_DataUpdate].dbo.Task 	
SET LastModifiedByID = @@DataUser
WHERE LastModifiedByID IS NULL



-- Fill in Contact where not connected to Oppty
UPDATE [Migration_DataUpdate].dbo.Task
SET WhatID = c.Id
FROM [Migration_DataUpdate].dbo.Task t
INNER JOIN SALESFORCE_PROD.dbo.Contact c ON c.ContactID__c = t.LegacyContactID
WHERE t.WhatID IS NULL


-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Task'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Task', 'External_Id__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Task'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[Task] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Task a
JOIN SALESFORCE_PROD.dbo.Task aa on aa.[External_Id__c] = a.[External_Id__c]