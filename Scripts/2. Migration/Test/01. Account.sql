--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Account;
drop table Migration_DataUpdate.dbo.ClientAlias_Pivot;
drop table Migration_DataUpdate.dbo.EMAtRisk_ClientList;
drop table Migration_DataUpdate.dbo.FMAtRisk_ClientList;
drop table Migration_DataUpdate.dbo.EMAtRisk_UniqueLoad;
drop table Migration_DataUpdate.dbo.FMAtRisk_UniqueLoad;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
go
*/


-- Create Alias pivot table 
SELECT DISTINCT ClientID, Alias 
INTO Migration_DataUpdate.dbo.ClientAlias_Pivot 
FROM (Select ClientID, Alias, ROW_NUMBER() over(PARTITION BY ClientID Order By ClientID) AS COL2 From Migration_Source.dbo.ClientAlias) T
WHERE T.COL2 = 1


-- Create unique ClientID table for EM at Risk with ONLY 1 record
-- 107
SELECT ClientID
INTO Migration_DataUpdate.dbo.EMAtRisk_ClientList
FROM Migration_Source.dbo.Clients_EMAtRisk
GROUP BY ClientID
HAVING COUNT(*) = 1


-- Create unique ClientID table for FM at Risk with ONLY 1 record
-- 37
SELECT ClientID
INTO Migration_DataUpdate.dbo.FMAtRisk_ClientList
FROM Migration_Source.dbo.Clients_FMAtRisk
GROUP BY ClientID
HAVING COUNT(*) = 1


-- Create EM At Risk pivot table
-- 107
SELECT ce.ClientID
		,ce.CustEMRiskReasons
		,ce.CustEMDateBecameAtRisk
		,ce.CustEMRiskLevels
		,ce.CustEMAnnualRevenueAtRisk
		,ce.CustEMAtRiskOutcome
		,ce.CustEMAtRiskOutcomeDate 
INTO Migration_DataUpdate.dbo.EMAtRisk_UniqueLoad 
FROM Migration_Source.dbo.Clients_EMAtRisk ce
INNER JOIN Migration_DataUpdate.dbo.EMAtRisk_ClientList ec on ec.ClientID = ce.ClientID


-- Create FM At Risk pivot table
-- 37
SELECT cf.ClientID
		,cf.CustFMRiskReason
		,cf.CustFMRiskLevels
		,cf.CustFMDateBecameAtRisk
		,cf.CustFMAnnualRevenueAtRisk
		,cf.CustFMAtRiskOutcome
		,cf.CustFMAtRiskOutcomeDate
INTO Migration_DataUpdate.dbo.FMAtRisk_UniqueLoad 
FROM Migration_Source.dbo.Clients_FMAtRisk cf
INNER JOIN Migration_DataUpdate.dbo.FMAtRisk_ClientList fc on fc.ClientID = cf.ClientID




-- Create staging table
-- 13408
-- 9
SELECT
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,CAST(c.ClientID as nvarchar(100)) as DeltekID__c
	,CAST(c.[Name] as nvarchar(80)) as [Name]
	,CASE WHEN c.[Type] = '9' THEN 'Government'
		WHEN c.[Type] = '11' THEN 'Unknown / Unclassified'
		WHEN c.[Type] = '13' THEN 'Corporation'
		WHEN c.[Type] = '15' THEN 'DMX Vendor'
		WHEN c.[Type] = '16' THEN 'Arts and Cultural'
		WHEN c.[Type] = '17' THEN 'Education'
		WHEN c.[Type] = '18' THEN 'Faith-Based'
		WHEN c.[Type] = '19' THEN 'Fraternal'
		WHEN c.[Type] = '20' THEN 'Healthcare'
		WHEN c.[Type] = '21' THEN 'Public Broadcasting'
		WHEN c.[Type] = '22' THEN 'Social Service'
		ELSE 'Unknown / Unclassified'
	END as Industry
	,CASE WHEN c.[Status] = 'A' THEN 'Active'
		WHEN c.[Status] = 'I' THEN 'Inactive'
		ELSE 'Active'
	END as Account_Status__c
	,CAST(c.Website as nvarchar(255)) as Website
	,CAST(c.CurrentStatus as nvarchar(100)) as [Type]
	,ca.Address as Address_Vision__c
	,ISNULL(ca.Address1, '') + CHAR(13)+CHAR(10) + ISNULL(ca.Address2, '') + CHAR(13)+CHAR(10) + ISNULL(ca.Address3, '') + CHAR(13)+CHAR(10) + ISNULL(ca.Address4, '') as BillingStreet
	,ca.City as BillingCity
	,CAST(ca.[State] as nvarchar(100)) as BillingState
	,ca.Zip as BillingPostalCode
	,CAST(ca.Country as nvarchar(100)) as BillingCountry
	,cla.Alias as Alias__c
	,CASE WHEN cctf.custAccreditationRegion = 'ACICS' THEN 'Independent Colleges and Schools'
		WHEN cctf.custAccreditationRegion = 'HLC' THEN 'The Higher Learning Commission'
		WHEN cctf.custAccreditationRegion = 'NCA' THEN 'North Central Association of Colleges and Schools'
		WHEN cctf.custAccreditationRegion = 'SACS' THEN 'Southern Association of Colleges and Schools'
		WHEN cctf.custAccreditationRegion = 'WASC' THEN 'Western Association of Schools and Colleges'
		WHEN cctf.custAccreditationRegion = 'Middle States' THEN 'Middle States Commission on Higher Education'
		WHEN cctf.custAccreditationRegion = 'New England' THEN 'New England Commission of Higher Education'
		WHEN cctf.custAccreditationRegion = 'Northwest' THEN 'Northwest Commission on Colleges and Universities'
		ELSE cctf.custAccreditationRegion
	END as Accreditation_Region__c
	,cctf.custNextAccreditationYear as Next_Accreditation_Year__c
	,cctf.custAnnualBudget as Annual_Budget__c
	,cctf.custAnnualEnrollment as Annual_Enrollment__c
	,cctf.custAnnualTuition as Annual_Tuition__c
	,CASE WHEN cctf.CustMarketBreakDown = '2' THEN '2 Yr Private'
		WHEN cctf.CustMarketBreakDown = '1' THEN '2 Yr Public'
		WHEN cctf.CustMarketBreakDown = '3' THEN '4 Yr Private'
		WHEN cctf.CustMarketBreakDown = '4' THEN '4 Yr Public'
		WHEN cctf.CustMarketBreakDown = '12' THEN 'Association'
		WHEN cctf.CustMarketBreakDown = '10' THEN 'Career % Private'
		WHEN cctf.CustMarketBreakDown = '13' THEN 'Corporation'
		WHEN cctf.CustMarketBreakDown = '9' THEN 'Government'
		WHEN cctf.CustMarketBreakDown = '8' THEN 'Military'
		WHEN cctf.CustMarketBreakDown = '5' THEN 'Private Graduate'
		WHEN cctf.CustMarketBreakDown = '6' THEN 'Public Graduate'
		WHEN cctf.CustMarketBreakDown = '7' THEN 'Secondary'
		WHEN cctf.CustMarketBreakDown = '14' THEN 'System Office'
		WHEN cctf.CustMarketBreakDown = '11' THEN 'Unknown'
		ELSE 'Unknown'
	END as Market_Breakdown__c
	,eu.CustEMRiskReasons as EM_Risk_Reason__c
	,eu.CustEMDateBecameAtRisk as EM_Date_Became_At_Risk__c
	,eu.CustEMRiskLevels as EM_Risk_Levels__c
	,eu.CustEMAnnualRevenueAtRisk as EM_Annual_Revenue_At_Risk__c
	,eu.CustEMAtRiskOutcome as EM_At_Risk_Outcome__c
	,eu.CustEMAtRiskOutcomeDate as EM_Risk_Outcome_Date__c
	,fu.CustFMRiskReason as FM_Risk_Reason__c
	,fu.CustFMRiskLevels as FM_Risk_Levels__c
	,fu.CustFMDateBecameAtRisk as FM_Date_Became_At_Risk__c
	,fu.CustFMAnnualRevenueAtRisk as FM_Annual_Revenue_At_Risk__c
	,fu.CustFMAtRiskOutcome as FM_At_Risk_Outcome__c
	,fu.CustFMAtRiskOutcomeDate as FM_Risk_Is_Outcome_Date__c
	,c.CreateDate as CreatedDate
	,u.Id as CreatedByID
	,c.ModDate as LastModifiedDate
	,u2.Id as LastModifiedByID
INTO [Migration_DataUpdate].dbo.Account 	
FROM [Migration_Source].dbo.CL c
INNER JOIN Migration_DataUpdate.dbo.CL_Pivot cp ON cp.ClientID = c.ClientID
INNER JOIN Migration_Source.dbo.CLAddress ca ON ca.ClientID = c.ClientID 
LEFT JOIN Migration_DataUpdate.dbo.ClientAlias_Pivot cla on cla.ClientID = c.ClientID
LEFT JOIN Migration_Source.dbo.ClientCustomTabFields cctf on cctf.ClientID = c.ClientID
LEFT JOIN Migration_DataUpdate.dbo.EMAtRisk_UniqueLoad eu on eu.CLientID = c.ClientID
LEFT JOIN Migration_DataUpdate.dbo.FMAtRisk_UniqueLoad fu on fu.CLientID = c.ClientID
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.EmployeeID__c = c.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = c.ModUser
WHERE ca.PrimaryInd = 'Y' 
AND ca.Country IN ('US','CA') 
AND c.[Status] = 'A' 
AND c.[Name] NOT LIKE '%SampleCC%' 
AND c.[Name] NOT LIKE '%DemoCC%' 
AND c.[Name] NOT LIKE '%DemoU%' 
AND c.[Name] NOT LIKE '%Demonstration%'  
AND c.[Name] NOT LIKE '%Ruffalo%'  
AND c.ClientID <> 'SSPENGLE1150496925699'
AND c.ClientID 
	in ('586E05E6DB404D62A1882FCFF5C463AF',
	'000080603B734F2D87FCCAD1F42037A4',
	'2B339ACCCC74435FA57B96A5E39198E6',
	'D844BF1149904BD392192FAD60FF5B1A',
	'AF3F6D06C7FE47C0A1A8D3D851F3E849'
	,'SSPENGLE1100032957147'
	,'Merge01_CP225629986'
	,'Merge01_CP255265225'
	,'ADVCLIENT002914'
	,'ADVCLIENT002791')
ORDER BY c.ClientID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Account
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix system users if null
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Account 	
SET CreatedByID = @@DataUser
WHERE CreatedByID IS NULL

UPDATE [Migration_DataUpdate].dbo.Account 	
SET LastModifiedByID = @@DataUser
WHERE LastModifiedByID IS NULL

-- Fix US Country
UPDATE [Migration_DataUpdate].dbo.Account 	
SET BillingCountry = 'United States'
WHERE BillingCountry = 'US'

-- Fix US States
UPDATE [Migration_DataUpdate].dbo.Account 	
SET BillingState = 
CASE WHEN BillingState = 'AL' THEN 'Alabama'
	WHEN BillingState = 'AK' THEN 'Alaska'
	WHEN BillingState = 'AZ' THEN 'Arizona'
	WHEN BillingState = 'AR' THEN 'Arkansas'
	WHEN BillingState = 'CA' THEN 'California'
	WHEN BillingState = 'CO' THEN 'Colorado'
	WHEN BillingState = 'CT' THEN 'Connecticut'
	WHEN BillingState = 'DE' THEN 'Delaware'
	WHEN BillingState = 'FL' THEN 'Florida'
	WHEN BillingState = 'GA' THEN 'Georgia'
	WHEN BillingState = 'HI' THEN 'Hawaii'
	WHEN BillingState = 'ID' THEN 'Idaho'
	WHEN BillingState = 'IL' THEN 'Illinois'
	WHEN BillingState = 'IN' THEN 'Indiana'
	WHEN BillingState = 'IA' THEN 'Iowa'
	WHEN BillingState = 'KS' THEN 'Kansas'
	WHEN BillingState = 'KY' THEN 'Kentucky'
	WHEN BillingState = 'LA' THEN 'Louisiana'
	WHEN BillingState = 'ME' THEN 'Maine'
	WHEN BillingState = 'MD' THEN 'Maryland'
	WHEN BillingState = 'MA' THEN 'Massachusetts'
	WHEN BillingState = 'MI' THEN 'Michigan'
	WHEN BillingState = 'MN' THEN 'Minnesota'
	WHEN BillingState = 'MS' THEN 'Mississippi'
	WHEN BillingState = 'MO' THEN 'Missouri'
	WHEN BillingState = 'MT' THEN 'Montana'
	WHEN BillingState = 'NE' THEN 'Nebraska'
	WHEN BillingState = 'NV' THEN 'Nevada'
	WHEN BillingState = 'NH' THEN 'New Hampshire'
	WHEN BillingState = 'NJ' THEN 'New Jersey'
	WHEN BillingState = 'NM' THEN 'New Mexico'
	WHEN BillingState = 'NY' THEN 'New York'
	WHEN BillingState = 'NC' THEN 'North Carolina'
	WHEN BillingState = 'ND' THEN 'North Dakota'
	WHEN BillingState = 'OH' THEN 'Ohio'
	WHEN BillingState = 'OK' THEN 'Oklahoma'
	WHEN BillingState = 'OR' THEN 'Oregon'
	WHEN BillingState = 'PA' THEN 'Pennsylvania'
	WHEN BillingState = 'RI' THEN 'Rhode Island'
	WHEN BillingState = 'SC' THEN 'South Carolina'
	WHEN BillingState = 'SD' THEN 'South Dakota'
	WHEN BillingState = 'TN' THEN 'Tennessee'
	WHEN BillingState = 'TX' THEN 'Texas'
	WHEN BillingState = 'UT' THEN 'Utah'
	WHEN BillingState = 'VT' THEN 'Vermont'
	WHEN BillingState = 'VA' THEN 'Virginia'
	WHEN BillingState = 'WA' THEN 'Washington'
	WHEN BillingState = 'WV' THEN 'West Virginia'
	WHEN BillingState = 'WI' THEN 'Wisconsin'
	WHEN BillingState = 'WY' THEN 'Wyoming'
END



-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'Account'
-- exec SF_BulkOps 'Update:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'Account'
exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'Account', 'DeltekID__c'
  

-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Account'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [IX_DeltekID__c] ON [dbo].[Account] ([DeltekID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Account a
JOIN SALESFORCE_PROD.dbo.Account aa on aa.DeltekID__c = a.DeltekID__c