--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.OpportunityLineItem;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2' 
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity' 
go
*/


-- Create staging table
-- 92291
-- 471
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,ISNULL(op.OpportunityID, '') + '-' + ISNULL(op.Seq, '') as External_Id__c
	,o.Id as OpportunityId
	,p.Id as Product2Id
	,op.Seq as Seq__c
	,op.CustProductQty as Quantity
	--,op.CustProductListPrice as ListPrice
	,op.CustProductCurrency as Currency__c
	,CASE WHEN op.CustProductDiscount < 0 THEN 0.00
		ELSE op.CustProductDiscount
	 END as Discount
	 ,CASE WHEN CustProductTotal = 0.01000 THEN 0.00
		ELSE op.CustProductTotal
	 END as TotalPrice
	,op.CustProductDeliveryStartDate as Product_Delivery_Start_Date__c
	,op.CustProductDeliveryEndDate as Product_Delivery_End_Date__c
	,op.CustIncludeinSOW as Include_In_SOW__c
	,op.CustRenewal as Renewal__c
	,op.CustContractYear as Contract_Year__c
	,op.CustYearofContract as Year_of_Contract__c
INTO [Migration_DataUpdate].dbo.OpportunityLineItem 	
FROM [Migration_Source].dbo.Opportunities_Products op
INNER JOIN SALESFORCE_PROD.dbo.Opportunity o on o.OpportunityID__c = op.OpportunityID
INNER JOIN SALESFORCE_PROD.dbo.Product2 p on p.ExternalId = op.custProduct
ORDER BY op.OpportunityID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].OpportunityLineItem
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem'
exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem', 'External_Id__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[OpportunityLineItem] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.OpportunityLineItem a
JOIN SALESFORCE_PROD.dbo.OpportunityLineItem aa on aa.External_Id__c = a.External_Id__c

