--SET NOCOUNT ON;

/*

-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'PricebookEntry' 
go

*/




-- Update staging table
-- 2046
UPDATE p
	SET p.IsActive = 
		CASE WHEN pp.IsActive = 'TRUE' THEN 'TRUE'
			ELSE 'FALSE'
		 END 
FROM [Migration_DataUpdate].dbo.PricebookEntry p	
JOIN [Migration_DataUpdate].dbo.Product_Pivot pp on pp.External_Id__c = p.External_Id__c




-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'PricebookEntry'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'PricebookEntry'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'PricebookEntry'