--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Contact;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
go
*/




-- Create staging table
-- 142061
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,c.ContactID as ContactID__c
	,a.Id as AccountId
	,CASE WHEN cpc.CustPrimary IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary__c
	,c.LastName as LastName
	,c.FirstName as FirstName
	,c.MiddleName as MiddleName
	,c.Salutation as Salutation
	,c.Suffix as Suffix
	,c.Title as Title
	,c.[Type] as Type__c
	,ISNULL(c.Address1, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address2, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address3, '') + CHAR(13)+CHAR(10) + ISNULL(c.Address4, '') as MailingStreet
	,c.City as MailingCity
	,CAST(c.[State] as nvarchar(100)) as MailingState
	,c.Zip as MailingPostalCode
	,CAST(c.Country as nvarchar(100)) as MailingCountry
	,c.Phone as Phone
	,c.Fax as Fax
	,c.CellPhone as MobilePhone
	,c.Email as Email
	,c.MailingAddress as Mailing_Address__c
	,c.Billing as Billing__c
	,CASE WHEN c.ContactStatus = 'A' THEN 'Active' 
		ELSE 'Inactive'
	 END as Status__c
	,c.PreferredName as Preferred_Name__c
	,CASE WHEN cctf.CustPrimaryEM IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary_EM__c
	,CASE WHEN cctf.CustPrimaryFM IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as Primary_FM__c
	,cctf.custActiveReference as Active_Reference__c
	,cctf.custNewtoposition as New_To_Position__c
	,CASE WHEN cctf.custNoEmail IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as No_Email__c
	,CASE WHEN cctf.custNoMail IS NOT NULL THEN 'TRUE'
		ELSE 'FALSE'
	 END as No_Mail__c
	,cctf.custNoEmailDate as No_Email_Date__c
	,cctf.custNoMailDate as No_Mail_Date__c
	,c.CreateDate as CreatedDate
	,u.Id as CreatedByID
	,c.ModDate as LastModifiedDate
	,u2.Id as LastModifiedByID
INTO [Migration_DataUpdate].dbo.Contact 	
FROM [Migration_Source].dbo.Contacts c
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = c.ClientID
--INNER JOIN Migration_DataUpdate.dbo.Account a ON a.DeltekID__c = c.ClientID   -- change this
-- dedupe the emails
LEFT JOIN Migration_Source.dbo.Clients_PrimaryContacts cpc on cpc.CustContact = c.ContactID
LEFT JOIN Migration_Source.dbo.ContactCustomTabFields cctf on cctf.ContactID = c.ContactID
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.Vision_Username__c = c.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.Vision_Username__c = c.ModUser
WHERE c.ContactStatus = 'A'
AND c.EMail IS NOT NULL
AND c.EMail NOT LIKE '%.rnl'
AND c.EMail NOT LIKE '%ruffalonl.com%'
AND c.[Type] != 'V'
ORDER BY c.ClientID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Contact
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix system users if null
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET CreatedByID = @@DataUser
WHERE CreatedByID IS NULL

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET LastModifiedByID = @@DataUser
WHERE LastModifiedByID IS NULL

-- Fix US Country
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET MailingCountry = 'United States'
WHERE MailingCountry = 'US'

-- Fix US States
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET MailingState = 
CASE WHEN MailingState = 'AL' THEN 'Alabama'
	WHEN MailingState = 'AK' THEN 'Alaska'
	WHEN MailingState = 'AZ' THEN 'Arizona'
	WHEN MailingState = 'AR' THEN 'Arkansas'
	WHEN MailingState = 'CA' THEN 'California'
	WHEN MailingState = 'CO' THEN 'Colorado'
	WHEN MailingState = 'CT' THEN 'Connecticut'
	WHEN MailingState = 'DE' THEN 'Delaware'
	WHEN MailingState = 'FL' THEN 'Florida'
	WHEN MailingState = 'GA' THEN 'Georgia'
	WHEN MailingState = 'HI' THEN 'Hawaii'
	WHEN MailingState = 'ID' THEN 'Idaho'
	WHEN MailingState = 'IL' THEN 'Illinois'
	WHEN MailingState = 'IN' THEN 'Indiana'
	WHEN MailingState = 'IA' THEN 'Iowa'
	WHEN MailingState = 'KS' THEN 'Kansas'
	WHEN MailingState = 'KY' THEN 'Kentucky'
	WHEN MailingState = 'LA' THEN 'Louisiana'
	WHEN MailingState = 'ME' THEN 'Maine'
	WHEN MailingState = 'MD' THEN 'Maryland'
	WHEN MailingState = 'MA' THEN 'Massachusetts'
	WHEN MailingState = 'MI' THEN 'Michigan'
	WHEN MailingState = 'MN' THEN 'Minnesota'
	WHEN MailingState = 'MS' THEN 'Mississippi'
	WHEN MailingState = 'MO' THEN 'Missouri'
	WHEN MailingState = 'MT' THEN 'Montana'
	WHEN MailingState = 'NE' THEN 'Nebraska'
	WHEN MailingState = 'NV' THEN 'Nevada'
	WHEN MailingState = 'NH' THEN 'New Hampshire'
	WHEN MailingState = 'NJ' THEN 'New Jersey'
	WHEN MailingState = 'NM' THEN 'New Mexico'
	WHEN MailingState = 'NY' THEN 'New York'
	WHEN MailingState = 'NC' THEN 'North Carolina'
	WHEN MailingState = 'ND' THEN 'North Dakota'
	WHEN MailingState = 'OH' THEN 'Ohio'
	WHEN MailingState = 'OK' THEN 'Oklahoma'
	WHEN MailingState = 'OR' THEN 'Oregon'
	WHEN MailingState = 'PA' THEN 'Pennsylvania'
	WHEN MailingState = 'RI' THEN 'Rhode Island'
	WHEN MailingState = 'SC' THEN 'South Carolina'
	WHEN MailingState = 'SD' THEN 'South Dakota'
	WHEN MailingState = 'TN' THEN 'Tennessee'
	WHEN MailingState = 'TX' THEN 'Texas'
	WHEN MailingState = 'UT' THEN 'Utah'
	WHEN MailingState = 'VT' THEN 'Vermont'
	WHEN MailingState = 'VA' THEN 'Virginia'
	WHEN MailingState = 'WA' THEN 'Washington'
	WHEN MailingState = 'WV' THEN 'West Virginia'
	WHEN MailingState = 'WI' THEN 'Wisconsin'
	WHEN MailingState = 'WY' THEN 'Wyoming'
END


-- Fix CA Country
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET MailingState = 'California', MailingCountry = 'United States'
WHERE MailingCountry = 'CA'



-- Fix Emails
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = ''
WHERE Email in ('don''t use', 'School Name-State-CS-PBC-CloseYear', 'RETIRED', 'Director of Alumni Relations', 'Recruiter', '804-796-4464', '508-830-5099', 'Program Manager � Phone Center', 'Institutional Research', 'Interim Vice President for Enrollment Management', 'Purchasing Agent', '512-452-4800') 


-- Fix specific emails
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'sdubeau@ucsd.edu'
WHERE Email = 'sdubeau.ucsd.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'alumni.eap@ucop.edu'
WHERE Email = 'alumni.eap.ucop.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'jashdown@adelphi.com'
WHERE Email = 'jashdown@adelphi'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'csosa@umd.edu'
WHERE Email = 'csosa.umd.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'pamela.bacon@utsa.com'
WHERE Email = 'pamela.bacon@utsa'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'alisha.kapp@blackburn.edu'
WHERE Email = 'alisha.kapp@blackburnedu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'cfraone@atlantic.edu'
WHERE Email = 'cfraone@atlanticedu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'tduerso1depaul@something.edu'
WHERE Email = 'tduerso1depaul.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'dpaulhamus@something.com'
WHERE Email = 'dpaulhamus@'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'wpaige@dioceseofprovidence.edu'
WHERE Email = 'wpaige@dioceseofprovidence'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'aboar@cc-md.edu'
WHERE Email = 'aboar@cc-md'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'jljohnson2@edmc.edu'
WHERE Email = 'jljohnson2edmc.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'ngrossbart@fit.edu'
WHERE Email = 'ngrossbart.fit.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'alanderson8@waketech.edu'
WHERE Email = 'alanderson8@ waketech'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'Ann.Unterreiner@asu.edu'
WHERE Email = 'Ann.Unterreiner.asu.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'mkmurray3@eiu.edu'
WHERE Email = 'mkmurray3eiu.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'albarea@lon.edu'
WHERE Email = 'albarea@lon'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = ''
WHERE Email = 'ljcope@w.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'treckart@mgc.edu'
WHERE Email = 'treckart.mgc.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'Tanisha.Latimer@gvltec.edu'
WHERE Email = 'Tanisha.Latimer@gvltec'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'albarea@lone.edu'
WHERE Email = 'albarea@lone'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'blank@blank.com'
WHERE Email = ''


-- Fix the issue with bad XML characters
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = [dbo].[removeinvalidunicode](Email, '')


-- Fix odd email text
UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = REPLACE(Email, ' ', '')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'president@swarthmore.edu'
WHERE Email = 'emailpresident@swarthmore.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = REPLACE(Email, '''', '')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'smedal@scu.edu'
WHERE Email = 'Sarah Medal <smedal@scu.edu>'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = REPLACE(Email, 'mailto:', '')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = REPLACE(Email, '@@', '@')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = REPLACE(Email, '?', '')

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'parillo@brandeis.edu'
WHERE Email = 'Matthew Parillo <parillo@brandeis.edu>'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'dmungle@ou.edu'
WHERE Email = 'dmungle@ou.edu.'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'cschrader@kbcc.cuny.edu'
WHERE Email = 'cschrader@kbcc..cuny.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'mccormick@kuf.org'
WHERE Email = 'Michaelean McCormick <mccormick@kuf.org>'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'althompson@sjcny.edu'
WHERE Email = 'althompson@sjcny_2.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'growe@sjcny.edu'
WHERE Email = 'growe@sjcny_2.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'paul.keith@cbshouston.edu'
WHERE Email = 'paul,keith@cbshouston.edu'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'wedigj@ihs.org'
WHERE Email = 'wedigj@ihs.org.'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'afellegy@fdltcc.edu'
WHERE Email = 'afellegy@fdltcc.edu.'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'smedal@scu.edu'
WHERE Email = 'SarahMedal<smedal@scu.edu>'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'parillo@brandeis.edu'
WHERE Email = 'MatthewParillo<parillo@brandeis.edu>'

UPDATE [Migration_DataUpdate].dbo.Contact 	
SET Email = 'mccormick@kuf.org'
WHERE Email = 'MichaeleanMcCormick<mccormick@kuf.org>'



-- Fix odd countryies again
UPDATE [Migration_DataUpdate].dbo.Contact
SET MailingCountry = 'United States'
WHERE MailingCountry IN 
('UM',
'MP',
'UM',
'EG',
'IT',
'GB',
'JP',
'VI',
'DM',
'UM',
'MP',
'DE',
'CN',
'UM',
'EG',
'AS',
'GE',
'AF',
'UM',
'PK',
'LC',
'UM',
'FR',
'FR',
'UM',
'UM',
'UM',
'UM',
'MP',
'MH',
'MP',
'PR',
'GB',
'VI',
'VI',
'VI',
'VI',
'UM',
'IN',
'IN',
'GB',
'IN',
'GB',
'NL',
'BR',
'AS',
'AU',
'NZ',
'AU',
'SG',
'AU',
'LB',
'AU',
'AU',
'SG',
'LB',
'CH',
'SG',
'SG',
'NZ',
'SG',
'SG',
'CH',
'CH',
'SG',
'AU',
'SG',
'AU',
'AU',
'AU',
'BS',
'CH')






-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact'
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact'
exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact_fix', 'ContactID__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Contact'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [ContactID__c] ON [dbo].[Contact] ([ContactID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Contact a
JOIN SALESFORCE_PROD.dbo.Contact aa on aa.ContactID__c = a.ContactID__c