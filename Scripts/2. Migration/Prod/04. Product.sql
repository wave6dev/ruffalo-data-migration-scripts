--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Product2;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2' 
go
*/




-- Create staging table
-- 1664
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,p.External_Id__c + '-' + p.ListPrice as External_Id__c
	,p.[Name] as [Name]
	,CAST('TRUE' as nvarchar(20)) as IsActive		-- true to load in inactivate later
	/*,CASE WHEN p.[Status] = 'Active' THEN 'TRUE'
		ELSE 'FALSE'
	 END as IsActive*/
INTO [Migration_DataUpdate].dbo.Product2 	
FROM [Migration_DataUpdate].dbo.Product_Pivot p
ORDER BY p.External_Id__c




-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Product2'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Product2'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[Product2] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Product2 a
JOIN SALESFORCE_PROD.dbo.Product2 aa on aa.External_Id__c = a.External_Id__c

