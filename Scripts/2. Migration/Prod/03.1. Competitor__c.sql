--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Competitor__c;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Competitor__c' 
go
*/




-- Create staging table
-- 249
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,UDIC_UID as External_Id__c
	,LEFT(ISNULL(CustName,''), 80) as [Name]
	,ISNULL(CustName,'') as Name_Full__c
INTO [Migration_DataUpdate].dbo.Competitor__c 	
FROM [Migration_Source].dbo.Competitor
ORDER BY UDIC_UID


-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Competitor__c
ADD [Sort] int identity (1,1)


-- Manual Updates


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Competitor__c'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Competitor__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Competitor__c'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[Competitor__c] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Competitor__c a
JOIN SALESFORCE_PROD.dbo.Competitor__c aa on aa.External_Id__c = a.External_Id__c

