SET NOCOUNT ON;

-- Drop staging tables
drop table Migration_DataUpdate.dbo.[User]

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User'
exec SF_Replicate 'SALESFORCE_PROD', 'UserRole'
exec SF_Replicate 'SALESFORCE_PROD', 'Profile'
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType'
go



DECLARE @@Contracting as nchar(18) = (SELECT Id FROM [SALESFORCE_PROD].dbo.[Profile] WHERE [Name] = 'Contracting')
DECLARE @@Finance as nchar(18) = (SELECT Id FROM [SALESFORCE_PROD].dbo.[Profile] WHERE [Name] = 'Finance')
DECLARE @@SalesProfile as nchar(18) = (SELECT Id FROM [SALESFORCE_PROD].dbo.[Profile] WHERE [Name] = 'Sales Profile')

DECLARE @@Role as nchar(18) = (SELECT Id FROM [SALESFORCE_PROD].dbo.[UserRole] WHERE [Name] = 'EVP Sales')



-- Create staging table
SELECT
	 CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,e.Employee as EmployeeID__c
	,CASE WHEN e.LastName IS NOT NULL THEN e.LastName 
		ELSE '(blank)'
	 END as LastName	
	,e.FirstName as FirstName
	,e.MiddleName as Middle_Name__c
	,ISNULL(e.EMail, '') + '.invalid' as Email
	,e.EMail as Username
	,@@SalesProfile as ProfileID
	,@@Role as UserRoleID
	,'TRUE' as IsActive
	,LEFT(ISNULL(e.FirstName,''), 1) + LEFT(ISNULL(e.LastName,''), 4) as Alias
	,LEFT(ISNULL(e.FirstName,'') + '.' +  ISNULL(e.LastName,''), 40) as Nickname
	,isnull(e.Address1, '') + CHAR(13)+CHAR(10) + isnull(e.Address2, '') + CHAR(13)+CHAR(10) + isnull(e.Address3, '') as Street
	,e.City as City
	,e.[State] as [State]
	,e.ZIP as PostalCode
	,e.Country as Country
	,e.HomePhone as Phone
	,e.Fax as Fax
	,e.Salutation as Salutation
	,e.Suffix as Suffix
	,e.Title as Title
	,CASE WHEN ISNULL(e.WorkPhoneExt, '') != '' THEN e.WorkPhone + ' ' + e.WorkPhoneExt
		ELSE e.WorkPhone 
	END as Work_Phone__c
	,e.MobilePhone as Mobile
	,'America/Chicago' as TimeZoneSidKey
	,'en_US' as LocaleSidKey
	,'ISO-8859-1' as EmailEncodingKey
	,'en_US' as LanguageLocaleKey
INTO [Migration_DataUpdate].dbo.[User]
FROM [Migration_Source].dbo.EM e
ORDER BY e.Employee



-- Insert/Update records 
use [Migration_DataUpdate] 
exec SF_BulkOps 'Insert', 'SALESFORCE_PROD', 'User'
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'User'
  
-- Replicate Data 
use SALESFORCE_PROD 
exec SF_Replicate 'SALESFORCE_PROD', 'User'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [IX_EmployeeID__c] ON [dbo].[User] ([EmployeeID__c]) INCLUDE ([Id])
GO
*/

-- Push back reality into staging table
UPDATE u
SET u.Id = u2.Id, Error = 'Operation Successful.'
FROM [Migration_DataUpdate].dbo.[User] u
JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = u.EmployeeID__c




-- FOR PROD
-- Update Email and Active
UPDATE u
SET u.Email = REPLACE(u.Email, '.invalid', ''), IsActive = 'TRUE'
FROM [Migration_DataUpdate].dbo.[User] u


-- Insert/Update records 
use [Migration_DataUpdate] 
exec SF_BulkOps 'Insert', 'SALESFORCE_PROD', 'User'
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'User'
  
-- Replicate Data 
use SALESFORCE_PROD 
exec SF_Replicate 'SALESFORCE_PROD', 'User'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [IX_EmployeeID__c] ON [dbo].[User] ([EmployeeID__c]) INCLUDE ([Id])
GO


-- Push back reality into staging table
UPDATE u
SET u.Id = u2.Id, Error = 'Operation Successful.'
FROM [Migration_DataUpdate].dbo.[User] u
JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = u.EmployeeID__c