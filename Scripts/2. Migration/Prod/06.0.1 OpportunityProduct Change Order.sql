--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.OpportunityLineItem_CO;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2' 
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity' 
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem' 
go
*/


-- Create staging table
-- 8254
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,ISNULL(olico.Opp_VisionID, '') 
		+ '-' + ISNULL(CAST(olico.VisionProduct_ID as varchar(100)), '') 
		+ '-' + ISNULL(CAST(olico.GRID_SalePrice as varchar(100)), '') 
		+ '-' + ISNULL(CAST(olico.GRID_YearOfContract as varchar(100)), '') 
		+ '-' + ISNULL(CAST(olico.GRID_StartDate as varchar(100)), '') 
		+ '-' + CAST(ROW_NUMBER ( ) OVER ( PARTITION BY olico.VisionProduct_ID, olico.GRID_SalePrice, olico.GRID_YearOfContract, olico.GRID_StartDate ORDER BY olico.VisionProduct_ID, olico.GRID_SalePrice, olico.GRID_YearOfContract, olico.GRID_StartDate) as varchar(100)) as External_Id__c
	,o.Id as OpportunityId
	,p.Id as Product2Id
	--,op.Seq as Seq__c
	,olico.GRID_Qty as Quantity
	--,op.CustProductListPrice as ListPrice
	,'USD' as Currency__c
	,CASE WHEN olico.GRID_Discount = 'null' THEN '0.00'
		ELSE olico.GRID_Discount 
	 END as Discount
	/* ,CASE WHEN olico.GRID_SalePrice = 0.01000 THEN 0.00
		ELSE olico.GRID_SalePrice
	 END as TotalPrice*/
	 ,CASE WHEN olico.GRID_Discount = 100 THEN 0.00
		ELSE olico.GRID_SalePrice
	 END as TotalPrice
	,CAST(olico.GRID_StartDate as datetime) as Product_Start_Date__c
	,CAST(olico.GRID_EndDate as datetime) as Product_End_Date__c
	,olico.GRID_IncludeInSOW as Include_In_SOW__c
	--, as Renewal__c
	--,olico.GRID_YearOfContract as Contract_Year__c
	,olico.GRID_YearOfContract as Year_of_Contract__c
INTO [Migration_DataUpdate].dbo.OpportunityLineItem_CO
FROM Migration_DataUpdate.dbo.OpportunityLineItem_ChangeOrder_Source olico
INNER JOIN SALESFORCE_PROD.dbo.Opportunity o on o.OpportunityID__c = olico.Opp_VisionID
--INNER JOIN SALESFORCE_PROD.dbo.Product2 p on SUBSTRING(p.External_Id__c,0,CharIndex('-', p.External_Id__c)) = olico.VisionProduct_ID
INNER JOIN SALESFORCE_PROD.dbo.Product2 p on p.External_Id__c = olico.VisionProduct_ID
											AND p.IsActive = 'TRUE'
ORDER BY olico.Opp_VisionID


-- Remove dupes
WITH cte AS (
    SELECT 
        External_Id__c, 
        ROW_NUMBER() OVER (
            PARTITION BY 
                External_Id__c
            ORDER BY 
                External_Id__c
        ) row_num
     FROM 
        [Migration_DataUpdate].[dbo].OpportunityLineItem_CO
)
DELETE FROM cte
WHERE row_num > 1;


-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].OpportunityLineItem_CO
ADD [Sort] int identity (1,1)

/*
-- Manual Updates
UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem
SET Discount = 0.00
WHERE Discount > 100.00

UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem
SET TotalPrice = 0.00
WHERE Discount = 100.00

UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem
SET Quantity = 1.00
WHERE Quantity = 0.00
*/

-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(500)', 'SALESFORCE_PROD', 'OpportunityLineItem_CO_fix'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem_CO', 'External_Id__c'
--exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem_CO'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[OpportunityLineItem] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.OpportunityLineItem_CO a
JOIN SALESFORCE_PROD.dbo.OpportunityLineItem aa on aa.External_Id__c = a.External_Id__c

