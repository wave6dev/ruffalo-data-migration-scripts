--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Project__c;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c' 
go
*/




-- Create staging table
-- 5544
-- LEFT JOIN 5544
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,a.Id as AccountID__c
	,p.ProjectName as [Name]
	,p.WBS1 as Project_Number__c
	,p.ClientID as ClientID
INTO [Migration_DataUpdate].dbo.Project__c
FROM [Migration_DataUpdate].dbo.Project_Pivot p
LEFT JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = p.ClientID
ORDER BY P.ClientID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Project__c
ADD [Sort] int identity (1,1)


-- Manual Updates

-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(10000),parallel', 'SALESFORCE_PROD', 'Project__c_fix'
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Project__c'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Project__c', 'Project_Number__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [Project_Number__c] ON [dbo].[Project__c] ([Project_Number__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Project__c a
JOIN SALESFORCE_PROD.dbo.Project__c aa on aa.Project_Number__c = a.Project_Number__c