SET NOCOUNT ON;
-- Replicate Data
use [SALESFORCE_PROD]

/*exec SF_Replicate 'SALESFORCE_PROD', 'User'
exec SF_Replicate 'SALESFORCE_PROD', 'UserRole'
exec SF_Replicate 'SALESFORCE_PROD', 'Profile'*/
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType'
exec SF_Replicate 'SALESFORCE_PROD', 'Account'
exec SF_Replicate 'SALESFORCE_PROD', 'Contact'
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'
exec SF_Replicate 'SALESFORCE_PROD', 'Product'
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityProduct'
exec SF_Replicate 'SALESFORCE_PROD', 'Pricebook'
exec SF_Replicate 'SALESFORCE_PROD', 'PricebookEntry'
exec SF_Replicate 'SALESFORCE_PROD', 'Task'
--exec SF_Replicate 'SALESFORCE_PROD', 'Attachment'
exec SF_Replicate 'SALESFORCE_PROD', 'Notes'
