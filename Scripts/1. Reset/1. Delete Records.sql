
-------------------------------------------------------------
-- User
-------------------------------------------------------------
-- Replicate Data
use [SALESFORCE_PROD]

exec SF_Refresh 'SALESFORCE_PROD', 'User', 'Yes'

-- Drop Table
drop table [Migration_DataUpdate].dbo.User_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.User_del
from [SALESFORCE_PROD].dbo.[User] a
where a.External_ID__c is not null

-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete(ns):bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'User_del'



-------------------------------------------------------------
-- Account
-------------------------------------------------------------

-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Account'


-- Drop Table
drop table [Migration_DataUpdate].dbo.Account_del


-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Account_del
from SALESFORCE_PROD.dbo.Account a
--where a.DeltekID__c is not null
--where a.OwnerId = '0051U000005gmRnQAI'


-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Account_del'



-------------------------------------------------------------
-- Project
-------------------------------------------------------------
use [SALESFORCE_PROD]

-- Replicate Data
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Project__c_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Project__c_del
from [SALESFORCE_PROD].dbo.Project__c a
/*where a.External_ID__c is not null
and a.lastname = '(blank)'
and a.recordtypeid = '01236000000fBmMAAU'*/

-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Project__c_del'



-------------------------------------------------------------
-- Contact
-------------------------------------------------------------
use [SALESFORCE_PROD]

-- Replicate Data
exec SF_Replicate 'SALESFORCE_PROD', 'Contact'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Contact_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Contact_del
from [SALESFORCE_PROD].dbo.Contact a
--where a.ContactID__c is not null
--and a.lastname = '(blank)'
--and a.recordtypeid = '01236000000fBmMAAU'

-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Contact_del'




-------------------------------------------------------------
-- Opportunity Product
-------------------------------------------------------------
-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem'

-- Drop Table
drop table [Migration_DataUpdate].dbo.OpportunityLineItem_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.OpportunityLineItem_del
from [SALESFORCE_PROD].dbo.OpportunityLineItem a
where a.External_ID__c is not null


-- Remove Records
use [Migration_DataUpdate]
--exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'OpportunityLineItem_del2'
exec SF_BulkOps 'HardDelete:bulkapi,batchsize(100)', 'SALESFORCE_PROD', 'OpportunityLineItem_del2'


-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem'



-------------------------------------------------------------
-- Opportunity
-------------------------------------------------------------
-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Opportunity_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Opportunity_del
from [SALESFORCE_PROD].dbo.Opportunity a
--where a.External_ID__c is not null
where a.CreatedById = '0051U000005gmRnQAI'


-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Opportunity_del'


-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'



-------------------------------------------------------------
-- Product
-------------------------------------------------------------
use [SALESFORCE_PROD]
-- Replicate Data
exec SF_Replicate 'SALESFORCE_PROD', 'Product2'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Product2_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Product2_del
from [SALESFORCE_PROD].dbo.Product2 a
where a.External_Id__c is not null


-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Product2_del'


-- Replicate Data
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2'



-------------------------------------------------------------
-- Task
-------------------------------------------------------------
use [SALESFORCE_PROD]

-- Replicate Data
exec SF_Replicate 'SALESFORCE_PROD', 'Task'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Task_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Task_del
from [SALESFORCE_PROD].dbo.Task a
where a.External_ID__c is not null
--and a.lastname = '(blank)'
--and a.recordtypeid = '01236000000fBmMAAU'

-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Task_del'




-------------------------------------------------------------
-- Competitor__c
-------------------------------------------------------------
use [SALESFORCE_PROD]

-- Replicate Data
exec SF_Replicate 'SALESFORCE_PROD', 'Competitor__c'

-- Drop Table
drop table [Migration_DataUpdate].dbo.Competitor__c_del

-- Find Records
select a.ID
, Cast('' as nvarchar(255)) as Error
into [Migration_DataUpdate].dbo.Competitor__c_del
from [SALESFORCE_PROD].dbo.Competitor__c a
where a.External_ID__c is not null
--and a.lastname = '(blank)'
--and a.recordtypeid = '01236000000fBmMAAU'

-- Remove Records
use [Migration_DataUpdate]
Exec SF_BulkOps 'HardDelete:bulkapi,parallel,batchsize(2000)', 'SALESFORCE_PROD', 'Competitor__c_del'