--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.OpportunityLineItem_datefix;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Product2' 
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity' 
go
*/


-- Create staging table
-- 132157
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,ISNULL(op.OpportunityID, '') + '-' + ISNULL(op.Seq, '') + '-' + ISNULL(CAST(op.CustProductTotal as varchar(30)), '') as External_Id__c
	,op.CustProductDeliveryStartDate as Product_Start_Date__c
	,op.CustProductDeliveryEndDate as Product_End_Date__c
INTO [Migration_DataUpdate].dbo.OpportunityLineItem_datefix 	
FROM [Migration_Source].dbo.Opportunities_Products op
INNER JOIN SALESFORCE_PROD.dbo.Opportunity o on o.OpportunityID__c = op.OpportunityID
INNER JOIN SALESFORCE_PROD.dbo.Product2 p on SUBSTRING(p.External_Id__c,0,CharIndex('-', p.External_Id__c)) = op.custProduct
ORDER BY op.OpportunityID


-- Remove dupes
WITH cte AS (
    SELECT 
        External_Id__c, 
        ROW_NUMBER() OVER (
            PARTITION BY 
                External_Id__c
            ORDER BY 
                External_Id__c
        ) row_num
     FROM 
        [Migration_DataUpdate].[dbo].OpportunityLineItem
)
DELETE FROM cte
WHERE row_num > 1;


-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].OpportunityLineItem_datefix
ADD [Sort] int identity (1,1)


-- Manual Updates
UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem_datefix
SET Discount = 0.00
WHERE Discount > 100.00

UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem_datefix
SET TotalPrice = 0.00
WHERE Discount = 100.00

UPDATE [Migration_DataUpdate].[dbo].OpportunityLineItem_datefix
SET Quantity = 1.00
WHERE Quantity = 0.00


-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem_datefix'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'OpportunityLineItem', 'External_Id__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'OpportunityLineItem'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [External_Id__c] ON [dbo].[OpportunityLineItem] ([External_Id__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.OpportunityLineItem_datefix a
JOIN SALESFORCE_PROD.dbo.OpportunityLineItem aa on aa.External_Id__c = a.External_Id__c

