--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Contact_fix;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
go
*/




-- Create staging table
-- 142061
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,c.ContactID as ContactID__c
	,cctf.custMPCcode as Manpower_Code__c
INTO [Migration_DataUpdate].dbo.Contact_fix
FROM [Migration_Source].dbo.Contacts c
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = c.ClientID
--INNER JOIN Migration_DataUpdate.dbo.Account a ON a.DeltekID__c = c.ClientID   -- change this
-- dedupe the emails
LEFT JOIN Migration_Source.dbo.Clients_PrimaryContacts cpc on cpc.CustContact = c.ContactID
LEFT JOIN Migration_Source.dbo.ContactCustomTabFields cctf on cctf.ContactID = c.ContactID
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.Vision_Username__c = c.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.Vision_Username__c = c.ModUser
WHERE c.ContactStatus = 'A'
AND c.EMail IS NOT NULL
AND c.EMail NOT LIKE '%.rnl'
AND c.EMail NOT LIKE '%ruffalonl.com%'
AND c.[Type] != 'V'
ORDER BY c.ClientID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Contact_fix
ADD [Sort] int identity (1,1)





-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact'
exec SF_BulkOps 'Update:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact_fix'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(10000)', 'SALESFORCE_PROD', 'Contact_fix', 'ContactID__c'



-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Contact'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [ContactID__c] ON [dbo].[Contact] ([ContactID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Contact_fix a
JOIN SALESFORCE_PROD.dbo.Contact aa on aa.ContactID__c = a.ContactID__c