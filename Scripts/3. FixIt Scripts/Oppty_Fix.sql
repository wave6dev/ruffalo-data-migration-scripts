--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Opportunity_fix;
go

/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c' 
go
*/

/*
DECLARE @@stdPB as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.Pricebook2 WHERE [Name] = 'Standard Price Book')
DECLARE @@opRT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Opportunity')
DECLARE @@opCORT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Change Order')
*/

-- Create staging table
-- 63070
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,o.OpportunityID as OpportunityID__c
	,uOwner.Id as OwnerId
INTO [Migration_DataUpdate].dbo.Opportunity_fix
FROM [Migration_Source].dbo.Opportunity o
INNER JOIN SALESFORCE_PROD.dbo.Opportunity a ON a.OpportunityID__c = o.OpportunityID
/*INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = o.ClientID
LEFT JOIN SALESFORCE_PROD.dbo.Contact c on c.ContactID__c = o.ContactID
LEFT JOIN Migration_Source.dbo.OpportunityCustomTabFields occf on occf.OpportunityID = o.OpportunityID
LEFT JOIN SALESFORCE_PROD.dbo.Project__c p on p.Project_Number__c = occf.CustCustProjectPlaceholder*/
LEFT JOIN SALESFORCE_PROD.dbo.[User] uOwner on uOwner.EmployeeID__c = SUBSTRING(o.Principal, PATINDEX('%[^0]%', o.Principal+'.'), LEN(o.Principal))
WHERE a.OwnerId = '0051U000005gmRnQAI'
/*LEFT JOIN SALESFORCE_PROD.dbo.[User] ucew on ucew.EmployeeID__c = occf.CustEmailwriter
LEFT JOIN SALESFORCE_PROD.dbo.[User] u on u.EmployeeID__c = o.CreateUser
LEFT JOIN SALESFORCE_PROD.dbo.[User] u2 on u2.EmployeeID__c = o.ModUser*/
ORDER BY o.OpportunityID




-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Opportunity_fix
ADD [Sort] int identity (1,1)




-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(1)', 'SALESFORCE_PROD', 'Opportunity'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(1)', 'SALESFORCE_PROD', 'Opportunity', 'OpportunityID__c'
exec SF_BulkOps 'Update:bulkapi,batchsize(200)', 'SALESFORCE_PROD', 'Opportunity_fix'




-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [OpportunityID__c] ON [dbo].[Opportunity] ([OpportunityID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Opportunity_fix a
JOIN SALESFORCE_PROD.dbo.Opportunity aa on aa.OpportunityID__c = a.OpportunityID__c