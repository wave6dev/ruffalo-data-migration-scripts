--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Opportunity_ChangeOrder_insert;
go



/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c' 
go
*/


DECLARE @@stdPB as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.Pricebook2 WHERE [Name] = 'Standard Price Book')
DECLARE @@opCORT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Change Order')


-- Create staging table
-- 63070
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,o.OpportunityID as OpportunityID__c
	,a.Id as AccountId
	,@@stdPB as Pricebook2Id
	,LEFT(o.[Name], 80) as [Name]
	,uOwner.Id as OwnerId
	,CASE WHEN o.OpportunityType = 'Cancellati' THEN 'Cancellation'
		WHEN o.OpportunityType = 'Extension' THEN 'Contract Extension'
		WHEN o.OpportunityType = 'Credit' THEN 'Credit'
		WHEN o.OpportunityType = 'CrossSell' THEN 'Cross-Sell'
		WHEN o.OpportunityType = 'New' THEN 'New Business'
		WHEN o.OpportunityType = 'PayAdjust' THEN 'Payment Schedule Adjustment'
		WHEN o.OpportunityType = 'Renewal' THEN 'Renewal'
		WHEN o.OpportunityType = 'RenewalUP' THEN 'Renewal Upsell'
		WHEN o.OpportunityType = 'SOWReplace' THEN 'SOW Replacement'
		WHEN o.OpportunityType = 'Terminatio' THEN 'Termination'
		WHEN o.OpportunityType = 'Upsell' THEN 'Upsell'
		WHEN o.OpportunityType = 'Zero Chang' THEN 'Zero Change'
		ELSE ''
	 END as [Type]
	,CASE WHEN o.Stage = 'LeadGen' THEN 'Lead Generating'
		WHEN o.Stage = 'Engaging' THEN 'Engaging'
		WHEN o.Stage = 'Value' THEN 'Planning for Value'
		WHEN o.Stage = 'DrvngClose' THEN 'Driving to Close'
		WHEN o.Stage = 'Committed' THEN 'Contract Negotiation'
		WHEN o.Stage = 'Won' THEN 'Won'
		WHEN o.Stage = 'Lost' Then 'Lost'
	 END as StageName
	,CAST(o.Revenue as decimal(19,4)) as Amount
	,CAST([dbo].[udf_StripHTML](o.[Description]) as nvarchar(MAX)) as [Description]
	,CAST(o.Probability as decimal(18,0)) as Probability
	--,o.OpenDate as start_date__c
	,GETDATE() as CloseDate
	--,ISNULL(o.CloseDate, occf.CustExpectedCloseDate) as Exp_Closed_Date__c
	,c.Id as Contact__c
	,@@opCORT as RecordTypeID
	,'TRUE' as Change_Order__c
INTO [Migration_DataUpdate].dbo.Opportunity_ChangeOrder_insert 	
FROM Migration_DataUpdate.dbo.Opportunity_ChangeOrder_Source o
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = o.ClientID
LEFT JOIN SALESFORCE_PROD.dbo.Contact c on c.ContactID__c = o.ContactID
INNER JOIN [Migration_DataUpdate].dbo.OpptyLineItem_CO_Owner_Pivot ocos on ocos.opp_VisionID = o.OpportunityID
LEFT JOIN Migration_DataUpdate.dbo.OpportunityCust_ChangeOrder_Source occf on occf.OpportunityID = o.OpportunityID
LEFT JOIN SALESFORCE_PROD.dbo.Project__c p on p.Project_Number__c = occf.CustProjectPlaceholder
LEFT JOIN SALESFORCE_PROD.dbo.[User] uOwner on uOwner.EmployeeID__c = o.Principal
ORDER BY ocos.Opp_VisionID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Opportunity_ChangeOrder_insert
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix null owner
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Opportunity_ChangeOrder_insert 	
SET OwnerId = @@DataUser
WHERE OwnerId IS NULL

UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Opportunity_ChangeOrder_insert a
JOIN SALESFORCE_PROD.dbo.Opportunity aa on aa.OpportunityID__c = a.OpportunityID__c

delete from Migration_DataUpdate.dbo.Opportunity_ChangeOrder_insert
where id != ''


-- Insert/Update records 
use [Migration_DataUpdate]
exec SF_BulkOps 'Insert:bulkapi,batchsize(5)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder_insert'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(1)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder_insert', 'OpportunityID__c'
--exec SF_BulkOps 'Update:bulkapi,batchsize(5)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder_insert'




-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [OpportunityID__c] ON [dbo].[Opportunity] ([OpportunityID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Opportunity_ChangeOrder_insert a
JOIN SALESFORCE_PROD.dbo.Opportunity aa on aa.OpportunityID__c = a.OpportunityID__c