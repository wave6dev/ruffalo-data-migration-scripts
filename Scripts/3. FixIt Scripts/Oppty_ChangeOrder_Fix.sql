--SET NOCOUNT ON;
-- Drop staging tables
drop table Migration_DataUpdate.dbo.Opportunity_ChangeOrder_fix;
drop table [Migration_DataUpdate].dbo.OpptyLineItem_CO_Owner_Pivot
go



/*
-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'User' 
exec SF_Replicate 'SALESFORCE_PROD', 'RecordType' 
exec SF_Replicate 'SALESFORCE_PROD', 'Account' 
exec SF_Replicate 'SALESFORCE_PROD', 'Contact' 
exec SF_Replicate 'SALESFORCE_PROD', 'Project__c' 
go
*/

/*
DECLARE @@stdPB as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.Pricebook2 WHERE [Name] = 'Standard Price Book')
DECLARE @@opCORT as nchar(18) = (SELECT Id FROM SALESFORCE_PROD.dbo.RecordType WHERE SobjectType = 'Opportunity' AND [Name] = 'Change Order')

*/


SELECT DISTINCT opp_VisionID, OppOwnerEM_ID
INTO  [Migration_DataUpdate].dbo.OpptyLineItem_CO_Owner_Pivot
FROM [Migration_DataUpdate].dbo.OpportunityLineItem_ChangeOrder_Source


-- Create staging table
-- 63070
SELECT 
	CAST('' as nchar(18)) as [ID]
	,CAST('' as nvarchar(255)) as Error
	,o.OpportunityID as OpportunityID__c
	,uOwner.Id as OwnerId
INTO [Migration_DataUpdate].dbo.Opportunity_ChangeOrder_fix
FROM Migration_DataUpdate.dbo.Opportunity_ChangeOrder_Source o
INNER JOIN SALESFORCE_PROD.dbo.Account a ON a.DeltekID__c = o.ClientID
INNER JOIN [Migration_DataUpdate].dbo.OpptyLineItem_CO_Owner_Pivot ocos on ocos.opp_VisionID = o.OpportunityID
/*LEFT JOIN SALESFORCE_PROD.dbo.Contact c on c.ContactID__c = o.ContactID
LEFT JOIN Migration_DataUpdate.dbo.OpportunityCust_ChangeOrder_Source occf on occf.OpportunityID = o.OpportunityID
LEFT JOIN SALESFORCE_PROD.dbo.Project__c p on p.Project_Number__c = occf.CustProjectPlaceholder*/
LEFT JOIN SALESFORCE_PROD.dbo.[User] uOwner on uOwner.EmployeeID__c = o.Principal
ORDER BY o.OpportunityID



-- Add Sort column
ALTER TABLE [Migration_DataUpdate].[dbo].Opportunity_ChangeOrder_fix
ADD [Sort] int identity (1,1)


-- Manual Updates
-- Fix null owner
DECLARE @@DataUser as nchar(18) = (SELECT TOP 1 Id FROM [SALESFORCE_PROD].dbo.[User] WHERE [LastName] = 'Data Admin')

UPDATE [Migration_DataUpdate].dbo.Opportunity_ChangeOrder_fix 	
SET OwnerId = @@DataUser
WHERE OwnerId IS NULL


-- Insert/Update records 
use [Migration_DataUpdate]
--exec SF_BulkOps 'Insert:bulkapi,batchsize(1000)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder_fix'
--exec SF_BulkOps 'Upsert:bulkapi,batchsize(1)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder', 'OpportunityID__c'
exec SF_BulkOps 'Update:bulkapi,batchsize(200)', 'SALESFORCE_PROD', 'Opportunity_ChangeOrder_fix3'




-- Replicate Data 
use [SALESFORCE_PROD]
exec SF_Replicate 'SALESFORCE_PROD', 'Opportunity'


USE [SALESFORCE_PROD]
GO
CREATE NONCLUSTERED INDEX [OpportunityID__c] ON [dbo].[Opportunity] ([OpportunityID__c]) INCLUDE ([Id])
GO


UPDATE a
SET a.Id = aa.Id, a.Error = 'Operation Successful.'
FROM Migration_DataUpdate.dbo.Opportunity_ChangeOrder_fix a
JOIN SALESFORCE_PROD.dbo.Opportunity aa on aa.OpportunityID__c = a.OpportunityID__c